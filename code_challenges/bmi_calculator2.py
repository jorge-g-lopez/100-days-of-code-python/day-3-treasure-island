# BMI calculator version 2

height = input("Enter your height in m: ")
weight = input("Enter your weight in kg: ")

bmi = round(float(weight) / (float(height) ** 2))

if bmi < 18.5:
    bmitext = "are underweight"
elif bmi >= 18.5 and bmi < 25:
    bmitext = "have a normal weight"
elif bmi >=25 and bmi < 30:
    bmitext = "are overweight"
elif bmi >= 30 and bmi < 35:
    bmitext = "are obese"
else:
    bmitext = "are clinically obese"

print (f"Your BMI is {bmi}, you {bmitext}.")