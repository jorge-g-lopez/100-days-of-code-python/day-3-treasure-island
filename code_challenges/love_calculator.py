# Love calculator

print("Welcome to the Love Calculator!")
name1 = input("What is your name? \n")
name2 = input("What is their name? \n")

names = (name1+name2).lower()

digit1  = names.count("t")
digit1 += names.count("r")
digit1 += names.count("u")
digit1 += names.count("e")

digit2  = names.count("l")
digit2 += names.count("o")
digit2 += names.count("v")
digit2 += names.count("e")

score = int(str(digit1)+str(digit2))

if score < 10 or score > 90:
    print(f"Your score is {score}, you go together like coke and mentos.")
elif score >= 40 and score <= 50:
    print(f"Your score is {score}, you are alright together.")
else:
    print(f"Your score is {score}.")